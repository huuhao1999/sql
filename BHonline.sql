create database BHTT
go 
use BHTT
go
create table ThanhVien(
MaLoai char(3),
MaKH char(6),
TenLoai nvarchar(255),
DieuKien int
)
create table KhachHang(
MaKH char(6),
MaLoai char(3),
HoTen nvarchar(255),
NgaySinh datetime,
GioiTinh nvarchar(255),
DiaChi nvarchar(255),
DiemTichLuy int
)
create table DonDatHang (
MaKH char(6),
MaSP char(6),
MaDH char(6),
DON_MaKH char(6),
DON_MaSP char(6),
CHI_MaDH char(6),
MaPhieu char(6),
TenNguoiNhan nvarchar(255),
SDTNguoiNhan char(10),
DiaChiNhanHang nvarchar(255),
TrangThaiDonHang nvarchar(255),
PhiVanChuyen money,
NgayGiao datetime,
ThanhTien money
)
create table ChiTiet_DonHang(
Don_MaKH char(6),
DON_MaSP char(6),
MaDH char(6),
MaPhieu char(6),
MaKH char(6),
MaSP char(6),
SL int,
DonGia int,
ThanhTien money,
NhanXet nvarchar(255)
)
create table PhieuDoiTra(
 MaPhieu char(6),
 Chi_MaPhieu char(6),
 NgayLap datetime
)
create table ChiTietDoiTra(
MaPhieu char(6),
TinhTrang nvarchar(255),
MaSPNEW char(6)
)
create table DaiLy
(
MaDL char(6),
TenDL nvarchar(255),
DiaChi nvarchar(255),
SDT char(10),
)
create table KinhDoanh(
MaDL char(6),
MaSP char(6),
SLTonKho int,
Price money
)
create table SanPham(
MaSP char(6),
MaSK char(6),
MaBH char(6),
MaMatHang char(6),
MaPhieu char(6),
TenSP nvarchar(255),
KhoiLuon float,
Moto nvarchar(255),
NamSX int
)
create table SuKienUuDai(
MaSK char(6),
TenSK nvarchar(255),
MucDoUuDai float
)
create table LoaiMatHang
(
MaMatHang char(6),
TenMatHang nvarchar(255)
)
create table CungCap
(
MaMatHang char(6),
MaNCC char(7),
MaMH char(6),
NCC char(7)
)

create table NhaCungCap
(
MaNCC char(7),
TenNCC nvarchar(255),
SDT char(10)
)
create table ChinhSachBH
(
MaBH char(6),
TenBH nvarchar(255),
HieuLucBH int
)





